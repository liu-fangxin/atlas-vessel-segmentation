## 基于 Caffe 框架的视网膜眼底血管分割 训练部署 

### 训练环境配置

-   cuda cudnn安装（需要有钱能买得起英伟达显卡）

    https://liufangxin.github.io/2019/05/06/ubuntu%E5%AE%89%E8%A3%85cuda8-0%E5%92%8Ccudnn6-0/#more

-   opencv opencv-contrib安装

    https://liufangxin.github.io/2019/05/06/ubuntu%E7%BC%96%E8%AF%91%E5%AE%89%E8%A3%85opencv%E5%92%8Copencv-contrib/#more

### 目录结构


-   data  目录下存放训练图片 
    caffemodel  目录下保存模型权重 
    train.sh  开始网络训练脚本 
    test.py  测试脚本 
    train.prototxt      分割网络结构文件 
    solver.prototxt    超参数配置文件 
    deploy.prototxt  网络部署文件 

-   训练流程

    1） 从 https://github.com/guomugong/FFIA 下载 Caffe，并编译，安装编译可参考本人博客（https://liufangxin.github.io/2019/05/06/ubuntu%E5%AE%89%E8%A3%85caffe/#more)

    2） 修改 train.sh，指定编译好的 caffe 二进制文件所在路径，指定 GPU ID 
    3） 如果有需要，可以修改 solver.prototxt，自行设置超参数， 
    4） 运行 train.sh，开始训练 
    5） GTX 1080ti 上大约 10 分钟即可完成 20000 次迭代。 

-   测试流程

    1） 修改 test.py 第 8 行，指定 caffe root 
    2） python test.py，执行结束后，分割结果保存到 test_velseg.png 