import numpy as np
import scipy.misc
from PIL import Image
import scipy.io
import scipy
import sys

caffe_root = '/home/guosong/DenoiseNet/'   # caffe root
sys.path.insert(0, caffe_root+'/python/')
import caffe


caffe.set_mode_gpu()
caffe.set_device(2)

net_struct  = './deploy.prototxt'
test_img    = './data/DRIVE/img/01_test.jpg'
weight_file = 'caffemodel/_iter_20000.caffemodel' 

# load net
net = caffe.Net(net_struct, weight_file, caffe.TEST);
    
im = Image.open(test_img)
in_ = np.array(im, dtype=np.float32)
in_ = in_[:,:,::-1] #BGR
in_ = in_.transpose((2,0,1))

#Reshape data layer
net.blobs['data'].reshape(1, *in_.shape)
net.blobs['data'].data[...] = in_

#Score the model
net.forward()

scipy.misc.imsave('test_velseg.png', net.blobs['output'].data[0][0,:,:])
