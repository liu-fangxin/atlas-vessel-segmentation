## 眼底视网膜血管分割系统部署说明

*Author：刘方鑫  Date: 2020.6.8*



本系统分为client和server两个部分，其中client部署在本地电脑上，server部署在搭载Atlas300的云服务器上

### Client 部署

客户端采用python3实现，运行所需的库主要有tkinter，PIL，numpy和socket，运行前需要确认这些库的安装

可以使用如下命令安装依赖包，其余为自带模块：

```
pip3 install Pillow==4.0.0 numpy==1.16.3
```

并修改需要连接的服务器端的IP地址和端口号（ client3.0.py 代码中的xxx）。

```python
try:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.connect(('xxx.xx.x.xxx',xxxx))
except socket.error as msg:
	print(msg)
	sys.exit(1)
```

然后通过python运行此文件即可。

```
python3 ./clent3.0.py
```



### Server 部署

server文件夹下是服务器端部署的全部文件，拷贝至服务器上HwHiAiUser用户目录下，视情况而定可能需要重新转换模型和编译程序。

服务器测试环境是华为弹性云服务器，操作系统是CentOS7.6，软件版本是C72

转换模型的命令如下，在vessel2目录下执行。

```shell
atc --model=caffe_model/deploy_vel_ascend.prototxt --weight=caffe_model/vel_hw_iter_5000.caffemodel --framework=0 --output=model/deploy_vel --soc_version=Ascend310 --input_format=NCHW --input_fp16_nodes=data -output_type=FP32 --out_nodes=”output:0”
```

编译程序的过程如下：

```
cd build/intermediates/host
cmake ../../../src -DCMAKE_CXX_COMPILER=g++ -DCMAKE_SKIP_RPATH=TRUE

make
```

服务器程序也通过python3实现，用到的库主要有socket，threading，PIL和numpy。

可以使用如下命令安装依赖包，其余为自带模块：

```
pip3 install Pillow==4.0.0 numpy==1.16.3
```

运行前也需要修改sever/vessel2/server.py代码中的IP地址和端口号，注意此处的IP地址为服务器网卡的IP地址，而不是云服务器绑定的公网IP。

```
try:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(('xxx.xxx.x.xxx', xxxx))
    s.listen(10)
except socket.error as msg:
    print(msg)
    sys.exit(1)
```

使用python运行此程序即可，或直接执行startserver.sh脚本

```
python3 server.py
sh startserver.sh
```

