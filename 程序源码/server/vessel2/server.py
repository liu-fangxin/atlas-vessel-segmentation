#!/usr/bin/env python
# -*- coding=utf-8 -*-
# author: liufangxin
# date: 2020.6.8

import socket
import threading
import time
import sys
import os
import struct
import numpy as np
from PIL import Image

def socket_service():
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        s.bind(('192.168.0.103', 23456))
        s.listen(10)
    except socket.error as msg:
        print(msg)
        sys.exit(1)
    print ("Waiting...")

    while 1:
        conn, addr = s.accept()
        t = threading.Thread(target=deal_data, args=(conn, addr))
        t.start()

def deal_data(conn, addr):
    print ('Accept new connection from {0}'.format(addr))
    # receive image
    while 1:
        buf = conn.recv(1024)
        if buf:
            filesize=str(buf,encoding="utf-8")
            new_filename = './data/test.jpg'
            print ('file new name is {0}, filesize if {1}'.format(new_filename, filesize))

            recvd_size = 0
            filesize=int(filesize)
            fp = open(new_filename, 'wb')
            print ("start receiving...")
            while not recvd_size == filesize:
                if filesize - recvd_size > 1024:
                    data = conn.recv(1024)
                    recvd_size += len(data)
                else:
                    data = conn.recv(filesize - recvd_size)
                    recvd_size = filesize
                fp.write(data)
            fp.close()
            print ("end receive...")

            # image preprocess
            imgProcess(new_filename)

            # call Atlas to calculate
            callAtlas()

            # result process
            savepath='./out/result.jpg'
            resultProcess(savepath)

            # send result jpg back
            if os.path.isfile(savepath):
                res_size=os.stat(savepath).st_size
                conn.sendall(bytes(str(res_size),encoding='utf-8'))
                fp = open(savepath, 'rb')
                while 1:
                    data = fp.read(1024)
                    if not data:
                        print ('{0} file send over...'.format(savepath))
                        break
                    conn.send(data)
                print("send result success")
        # conn.close()
        # break

# image preprocess
def imgProcess(img_path):
    if os.path.isfile(img_path):
        im = Image.open(img_path)
        im = im.resize((512,512))
        # hwc
        img = np.array(im)
        # rgb to bgr
        img = img[:,:,::-1]

        shape = img.shape
        img = img.astype("float16")
        img = img.reshape([1] + list(shape))
        result = img.transpose([0, 3, 1, 2])

        outputName = img_path + ".bin"
        result.tofile(outputName)
        print("file saved to", outputName)
    else:
        print("img file is not exist")

# call Atlas to calculate
def callAtlas():
    old_path=os.getcwd();
    os.chdir(old_path+'/out')
    os.system("./main")
    os.chdir(old_path)
    print("cal success")

# result process
def resultProcess(savepath):
    resultfile='./out/output1_0.bin'
    if os.path.isfile(resultfile):
        result=[]
        with open(resultfile,'rb') as fin:
            while True:
                item=fin.read(4)
                if not item:
                    break
                elem=struct.unpack('f',item)[0]
                result.append(elem)
            fin.close()
        img=np.array(result)
        img=np.reshape(img*255,(512,512))

        # Save the result
        resultimage=Image.fromarray(np.uint8(img))
        resultimage.save(savepath)
        print("result save success")
    else:
        print("result file is not exist")

if __name__ == '__main__':
    socket_service()