clc
clear

path='../result/';

test_all=dir([ path '*.jpg']);

num_test_images=length(test_all);
label = zeros(1,584*565*num_test_images);
pred = zeros(1,584*565*num_test_images);
ii = 1;
for i=1:num_test_images
    image_filename=[path test_all(i).name];
    label_filename=['Drive/1st_manual_label/' test_all(i).name(1:2) '_manual1.gif'];
    mask_filename=['Drive/mask/' test_all(i).name(1:7) '_mask.gif'];
    la = imread(label_filename);
    im = imread(image_filename);
    ma = imread(mask_filename);
    im = imresize(im,[584 565]);
    im = double(im);
    for j=1:size(im,1)
        for kk=1:size(im,2)
         if ma(j,kk) == 255
            if la(j,kk) == 0
                label(1,ii) = 0;
            else
                label(1,ii) = 1;
            end
            pred(1,ii) = im(j,kk)/255;
            ii = ii+1;
          end
        end
    end
end
label=label(1,1:ii-1);
pred=pred(1,1:ii-1);
%   plotroc(label,pred);
[x y t auc op]=perfcurve(label,pred,1);
auc
% [auc_pr re pr]=calculate_pr(pred,label);

% [prec,tpr,fpr,thresh,auc_pr,auc_roc]=prec_rec(pred,label);