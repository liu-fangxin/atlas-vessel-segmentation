## 精度测试说明

此程序是计算DRIVE数据集分割结果的AUC指标

-   目录结构

    Drive: 第一专家标注文件

    compute_roc_resvise.m: 计算AUC的matlab脚本

-   使用方法

    1.修改matlab脚本中path变量为存放结果的路径

    ```
    path='F:\Projects\result';
    ```

    2.修改结果文件名称或脚本中的13行和14行的标注文件名，使其能对应上，才能读取对应的标注文件

    ```
        label_filename=['Drive/1st_manual_label/' test_all(i).name(9:10) '_manual1.gif'];
        mask_filename=['Drive/mask/' test_all(i).name(9:15) '_mask.gif'];
    ```

    使用matlab执行此程序即可，测试环境为matlab R2014a。

